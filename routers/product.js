const express = require('express');
const prodController = require('../controllers/product');
const auth = require('../auth');
const router = express.Router();

// add product admin only
router.post('/addProduct', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization) 

	if(userData.isAdmin !== true) {
		return res.send('Not Authorized.')
		
	} else {

		prodController.addProduct(req.body).then(resultFromController => {
			res.send(resultFromController)
		})		
	}

})

// retrieve all products
router.get('/all', auth.verify, (req,res) => {
	userData = auth.decode(req.headers.authorization)

	prodController.allProducts().then(resultFromController => res.send(resultFromController))
});

// retrieve a single product
router.get('/:id', auth.verify, (req,res) => {
	userData = auth.decode(req.headers.authorization)

	prodController.singleProduct(req.params.id).then(resultFromController => res.send(resultFromController))
});

// update a product (admin)
router.put('/:id/update', (req, res) => {
	userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin !== true) {
		return res.send('Not Authorized')

	} else {
		return prodController.updateProd(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
	}

})

// archive a product (admin)
router.put('/:id/archive', (req, res) => {
	userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin !== true) {
		return res.send('Not Authorized')

	}else {
		prodController.archiveProd(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
	}
})



//Schedule a shipment (admin) userid
router.post('/:id/shipment', auth.verify, (req, res) => {
	userData = auth.decode(req.headers.authorization)

	
	let data = {

		userId: req.params.id,
		productId: req.body.productId,
		orderId: req.body.orderId
	}

	if(userData.isAdmin === true){

		prodController.shipmentSched(data, req.params.id, req.body).then(resultFromController => res.send(resultFromController))
	} else {
		return res.send('Access Denied')
	}
})

module.exports = router;