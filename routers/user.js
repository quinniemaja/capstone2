const express = require('express');
const auth = require('../auth');
const userController = require('../controllers/user');
const orderController = require('../controllers/order');
const router = express.Router();


// register a user
router.post('/register', (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

// login user
router.post('/login', (req, res) => {

	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})

// setup admin user
router.put('/:id/setAsAdmin', auth.verify, (req,res) => {
	userData = auth.decode(req.headers.authorization)

	userController.setupAdmin(req.params.id).then(resultFromController => res.send(resultFromController))
});

//order (user)
router.post('/:id/createOrder', auth.verify, (req, res) => {
	userData = auth.decode(req.headers.authorization)

	let loginData = {

		userId: userData.id,
		productId: req.body.productId,
		qty: req.body.qty

	}

	if(userData.isAdmin == false && userData.id == req.params.id) {

		orderController.orderProd(loginData).then(resultFromController => {
			res.send(resultFromController)
		})

	} else {
		return res.send('You need to log in first')
	}

})

// retrieve user's order
router.get('/:id/myOrderList', auth.verify, (req, res) => {
	userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin == false && userData.id == req.params.id) {

		userController.UserOrderList(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
	} else {
		return res.send('Not Authorized')
	}
})

// retrieve all orders (admin) 

router.get('/admin/allOrders', auth.verify, (req,res) => {
	userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin === true) {

		userController.allOrders().then(resultFromController => res.send(resultFromController))
	}

})

// retrieve all first payments (admin) 

router.get('/admin/Payments', auth.verify, (req,res) => {
	userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin === true) {

		orderController.Payments().then(resultFromController => res.send(resultFromController))
	}

})



// update payment (user)
router.post('/:id/payments', auth.verify, (req, res) => {
	userData = auth.decode(req.headers.authorization)

	let data = {
		userId: userData.id,
		orderId: req.body.orderId,
		totalAmount: req.body.totalAmount
	}

	if(userData.isAdmin === false && userData.id === req.params.id){

		orderController.payment(data, req.params.id, req.body).then(resultFromController => res.send(resultFromController))
	} else {
		return res.send('Access Denied')
	}
})





module.exports = router;
