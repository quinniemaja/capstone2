const User = require('../models/userSchema');
const Product = require('../models/productSchema');
const Order = require('../models/order');
const auth = require('../auth');


// create order (user) 
module.exports.orderProd = async (loginData) => {

	// console.log(loginData)	
	  let product = await Product.findById(loginData.productId)	

	  if(product.qty >= loginData.qty) {

		  	// console.log(product)
		  let user = await User.findById(loginData.userId);
		  // console.log(user)
		  // pushing an order to order model.

		  let isOrderUpdated = await Order.find({}).then((result, err) => {
		  		 let newOrder = new Order({
		  		userId: user.id,
		  		username: user.username,
		  		productId: loginData.productId,
		  		productName: product.name,
		  		qty: loginData.qty,
		  		totalAmount: product.price*loginData.qty,


		 		 });

				  return newOrder.save().then((order, err) => {
				  	if (err) {
				  		console.log(err)
				  		return false
				  	} else {
				  		return true
				  	}
				  })


		  })
		 

		  let isUserUpdated = await User.findById(loginData.userId).then((user) => {

		  	user.order.push({
		  		productId: loginData.productId, 
		  		productName: product.name,
		  		qty: loginData.qty, 
		  		totalAmount: product.price*loginData.qty});
		  	// console.log(user);
		  	return user.save().then((user, err) => {

		  	
		  		if(err) {
		  			console.log(err)
		  			return false

		  		} else {
		  			return true
		  		}
		  	})
		  })

		  // console.log(isUserUpdated)

		  let isProductUpdated = await Product.findById(loginData.productId).then((prod) => {
		  		
		  		prod.order.push({
		  			userId: loginData.userId, 
		  			qty: loginData.qty});

		  		return prod.save().then((prod, err) => {
		  			if(err) {
		  				console.log(err)
		  				return false
		  			} else {
		  				return true
		  			}
		  		})
		  })


		  // Updating stocks && product status
		  const stocks = product.qty -= loginData.qty;
		  let prodStatus = await Product.findByIdAndUpdate(loginData.productId, {qty: stocks}).then((result, err) => {
		  	// console.log(result)
		  		if(err) {
		  			console.log(err)
		  			return false
		  		} else if(result.qty <= 0) {
		  			console.log(result.isActive)
		  			result.isActive = false
		  			return result.save().then((prod, err) => {
		  				if(prod) {
		  					console.log(prod)
		  					return false
		  				} else 
		  					console.log(err)
		  					return false
		  			})
		  		} else {
		  			return true
		  		}
		  	
		  })
		  // console.log(prodStatus)

		  if(isOrderUpdated && isProductUpdated && isUserUpdated && prodStatus) {
		  	return ('Order have succesfully created!')
		  } 
	  }  else {
	  	return ('Insufficient stocks')
	  }
	  
}

// update payment docs (user)
module.exports.payment = async (data) => {

	let userPayment = await User.findById(data.userId).then((user) => {

		user.payment.push({totalAmount: data.totalAmount, orderId: data.orderId});

		return user.save().then((user, err) => {
			// console.log(user);
			if(err) {
				console.log(err)
				return false
			} else {
				return true
			}
		})
	})
	console.log(data.orderId)
	let orderPayment = await Order.findById(data.orderId).then(order => {
		console.log(order)
		order.payment.push({totalAmount: data.totalAmount});

		return order.save().then((orderPayment, err) => {
			// console.log(payment.firstPayment)
			if(err) {
				console.log(err)
				return false

			} else {
				return true
			}
		})
	})

	if(userPayment && orderPayment) {
		return ('Payment have succesfully updated')
	} else {
		return false
	}
}