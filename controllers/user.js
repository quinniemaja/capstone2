const User = require('../models/userSchema');
const Product = require('../models/productSchema');
const Order = require('../models/order');
const bcrypt = require('bcrypt');
const auth = require('../auth');


//Register Users and check if username && email already exist

	module.exports.registerUser = async (reqBody) => {

  	let inputEmail = await User.findOne({email: reqBody.email}).then(email => {

  		if(email === null) {
  			return true
  		} else {
  			return false
  		}
  	})

	let inputUsername = await User.findOne({username: reqBody.username}).then(username => {

  		if (username === null) {
  			
  			return true
  		} else {
  			return false
  		}
  	})

  	if(inputEmail === true ) {

		if(inputUsername === true) {

				let newUser = new User({

				username: reqBody.username,
				email: reqBody.email,
				password: bcrypt.hashSync(reqBody.password, 10),
				firstName: reqBody.firstName,
				lastName: reqBody.lastName,
				mobileNo: reqBody.mobileNo,
				address: reqBody.address
			})

			return newUser.save().then((user, err) => {
				if(err) {
					console.log(err)
					return false 
				} else {
					return (`Welcome ${user.username}!`)
				}
			})

		} else {
			return (`Username already exist!`)
		}

  	} else {
  		return (`Email already exist!`)
  	}
}

//login user

module.exports.loginUser = async (reqBody) => {

	let username = await User.findOne({username: reqBody.username});

	let email = await User.findOne({email: reqBody.username});

		if(username === null) {

			if(email !== null) {

			// console.log(email)

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, email.password)

				if(isPasswordCorrect) {
					return { access: auth.createAccessToken(email)}

				} else {
					return ('Cannot fetch data, incorrect credentials');
				}
			} else {
				return ('Cannot fetch data, incorrect credentials');
			}		

		} else  {


			// console.log(username)
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, username.password)

			if(isPasswordCorrect) {
				return { access: auth.createAccessToken(username)}

			} else {
				return ('Cannot fetch data, incorrect credentials');
			}
		}
			

} 


// setting an Admin account

module.exports.setupAdmin = (userId) => {
	return User.findById(userId).then((result, err) => {
		console.log(userId);

			if(err) {
				console.log(err)
				return false
			} else {
				result.isAdmin = true
				return result.save().then((admin, err) => {
					if(err){
						console.log(err)
						return false
					} else {
						return true
					}
				})
			}

				
	})

}



// retrieve user's orderlist
module.exports.UserOrderList = (userId) => {

	return User.findById(userId).then((result, err) => {
		if(err) {
			console.log(err)
			return false
		} else {
			let orderDetails = {
				Status : result.status,
				orders: result.order,
				firstPayments: result.firstPayment,
				lastPayment: result.lastPayment,
				shipments: result.shipments
			}
			return orderDetails
		}
	})
}

//retrieve all orders (admin) 
module.exports.allOrders = () => {

	return User.find({isActive: true}).then((result, err) => {
		if(err) {
			console.log(err)
			return false
		} else {
			let orders = Object.values(result).map(user => user.order)
			return orders
		}
	})
}

//retrieve all first payment (admin) 
module.exports.allFirstPayments = () => {

	return User.find({}).then((result, err) => {
		if(err) {
			console.log(err)
			return false
		} else {
			
			let payments = Object.values(result).map(user => user.firstPayment)
			return payments
		}
	})
}

//retrieve all last payment (admin) 
module.exports.allLastPayments = () => {

	return User.find({}).then((result, err) => {
		if(err) {
			console.log(err)
			return false
		} else {
			
			let payments = Object.values(result).map(user => user.lastPayment)
			return payments
		}
	})
}



module.exports.lastPayment = async (data) => {

	let userPayment = await User.findById(data.userId).then((user) => {

		user.lastPayment.push({totalAmount: data.totalAmount, orderId: data.orderId});
		return user.save().then((payment, err) => {

			if(err) {
				console.log(err)
				return false
			} else {
				return true
			}
		})
	})

	let prodPayment = await Product.findById(data.productId).then(prod => {

		prod.lastPayment.push({orderId: data.orderId, userId: data.userId, totalAmount: data.totalAmount});

		return prod.save().then((payment, err) => {

			if(err) {
				console.log(err)
				return false

			} else {
				return true
			}
		})
	})

	if(userPayment && prodPayment) {
		return true
	} else {
		return false
	}
}


// update status a single order
module.exports.updateProdStatus = (data) => {

	return User.findById(data.userId).then((user, err) => {
		
		if(err) {
			console.log(err)
			return false
		} else {

				user.status = data.status
				return user.save().then((status, err) => {
					if(err){
						console.log(err)
						return false
					} else {
						return true
					}
				})
		}
		
	})

}