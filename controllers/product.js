const Product = require('../models/productSchema');
const Order = require('../models/order');
const auth = require('../auth');
const User = require('../models/userSchema');

// add product
module.exports.addProduct = (reqBody) => {

		let newProduct = new Product({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price,
			qty: reqBody.qty

		});

		return newProduct.save().then((prod, err) => {

			if(err) {
				console.log(err)
				return false

			} else {
				return (`${prod.name} have successfully added.`)
			}
		})

}

// retrieve all products
module.exports.allProducts = () => {

	return Product.find({isActive: true}).then(result => {
		return result
	})
}

// retrieve a single product thru id
module.exports.singleProduct = (prodId) => {
		console.log(prodId)
	return Product.findById(prodId).then((result, err) => {
		console.log(result)
				if (err) {
			console.log(err)
			return false
		} else{
			return result
		}
	})
}

// update a single product
module.exports.updateProd = (prodId, reqBody) => {

	let updatedProd = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price,
			qty: reqBody.qty

		}
		

	return Product.findByIdAndUpdate(prodId, updatedProd).then((result, err) => {

		if(err) {
			console.log(err)
			return false

		} else {
			return result
		}
		
	})
}

// // archive a product (admin)

// module.exports.archiveProd = (prodId) => {

// 	return Product.findById(prodId).then((result, err) => {

// 		if(err){
// 			console.log(err)
// 			return false
// 		}
// 		result.isActive = false
// 		return result.save().then((prod, err) => {
// 			if(err) {
// 				console.log(err)
// 				return false

// 			} else{
// 				return prod
// 			}
// 		})
// 	})
// }



// Sched an item shipment
module.exports.shipmentSched = async (data) => {

	let userShipment = await User.findById(data.userId).then((user) => {

		user.shipments.push({orderId: data.orderId});
		return user.save().then((delivery, err) => {

			if(err) {
				console.log(err)
				return false
			} else {
				return true
			}
		})
	})

	let prodShipment = await Product.findById(data.productId).then(prod => {

		prod.shipments.push({orderId: data.orderId, userId: data.userId});

		return prod.save().then((delivery, err) => {

			if(err) {
				console.log(err)
				return false

			} else {
				return true
			}
		})
	})

	if(userShipment && prodShipment) {
		return true
	} else {
		return false
	}
}
