const mongoose = require('mongoose');
const orderSchema = new mongoose.Schema({

	userId: {
		type: String,
		required: [true, 'userId is required']
	},
	username: {
		type: String,
		required: [true, 'username is required']
	},

	productId: {
		type: String,
		required: [true, 'productId is required.']
	},
	productName: {
		type: String,
		required: [true, 'productName is required.']
	},

	createdOn: {
		type: Date,
		default: new Date()
	},

	qty: {
		type: Number,
		required: [true, 'qty is required']
	},
	totalAmount: {
		type: Number,
		required: [true, 'amount is required']
	},
	status: {
		type: String,
		default: "Order confirmed"
	},

	payment: [{

			paidOn: {
				type: Date,
				default: new Date()
			},
			totalAmount: {
				type: Number,
				required: [true, 'Amount is required.']
			},

			status: {
				type: String,
				default: "Paid"
			}
		

	}],

	
	// shipments: [{

	// 		orderId: {
	// 			type: String,
	// 			required: [true, 'Order ID is required']
	// 		},

	// 		status: {
	// 			type: String,
	// 			default: 'Item shipped'
	// 		},

	// 		shippedOn: {
	// 			type: Date,
	// 			default: new Date()
	// 		},

	// }]
})

module.exports = mongoose.model('Order', orderSchema);