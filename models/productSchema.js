const mongoose = require('mongoose');

const prodSchema = new mongoose.Schema({

	name: {
		type: String,
		required: [true, 'Product name is required.']
	},

	description: {
		type: String,
		required: [true, 'description is required']
	},

	price: {
		type: String,
		required: [true, 'Price is required.']
	},

	qty: {
		type: Number,
		default: 0
	},

	createdOn: {
		type: Date,
		default: new Date()
	},

	isActive: {
		type: Boolean,
		default: true
	},


	order: [
			{
			userId: {
				type: String,
				required: [true, 'Product ID is required.']
			},

			orderedOn: {
				type: Date,
				default: new Date()
			},
			
			qty: {
				type: Number,
				required: [true, 'Quantity is required.']
			},

		}],	


});

module.exports = mongoose.model('Product', prodSchema);