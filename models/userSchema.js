const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({

	username: {
		type: String,
		required: [true, 'Username is required.']
	},

	email: {
		type: String,
		required: [true, 'Email is required.']
	},

	password: {
		type: String,
		required: [true, 'Password is required.']
	},

	firstName: {
		type: String,
		required: [true, 'First name is required.']
	},

	lastName: {
		type: String,
		required: [true, 'Last name is required.']
	},

	mobileNo: {
		type: String,
		required: [true, 'Mobile number is required']
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	address: {
		type: String,
		required: [true, 'Address is required.']
	},

	order: [
			{
			productId: {
				type: String,
				required: [true, 'Product ID is required.']
			},

			productName: {
				type: String,
				required: [true, 'Product ID is required.']
			},

			orderedOn: {
				type: Date,
				default: new Date()
			},
			
			qty: {
				type: Number,
				required: [true, 'Quantity is required.']
			},

			totalAmount: {
				type: Number,
				required: [true, 'total amount is required']
			},

			status: {
				type: String,
				default: "Order confirmed"  //Order/ or payment confirmed / out for delivery
			},

		}],	


	payment: [{

		orderId: {
			type: String,
			required: [true, 'orderid is required']
		},

		paidOn: {
			type: Date,
			default: new Date()
		},
		totalAmount: {
			type: Number,
			required: [true, 'Amount is required.']
		},

		status: {
			type: String,
			default: "Paid"
		}

	}],

// shipments: [{

// 			orderId: {
// 				type: String,
// 				required: [true, 'Order ID is required']
// 			},

// 			status: {
// 				type: String,
// 				default: 'Item shipped'
// 			},

// 			shippedOn: {
// 				type: Date,
// 				default: new Date()
// 			},

// 		}]

	
	
		
});

module.exports = mongoose.model('User', userSchema);