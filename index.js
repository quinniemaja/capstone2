const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const port = process.env.PORT || 4000;
const userRoutes = require('./routers/user');
const prodRoutes = require('./routers/product');

const app = express();

app.use(express.json())
app.use(express.urlencoded({extended: true}));
app.use(cors());

mongoose.connect('mongodb+srv://admin:admin131@bootcamp.y9mz6.mongodb.net/capstone02?retryWrites=true&w=majority', {

	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

	db.on("error", console.error.bind(console, "Connection Error"))
	db.once('open', () => console.log('Now connected to MongoDB Atlas'));

app.use('/user', userRoutes);
app.use('/products', prodRoutes);

app.listen(port, () => console.log(`Server is running at port: ${port}.`))


/*
	https://damp-cove-26715.herokuapp.com/ | 
	https://git.heroku.com/damp-cove-26715.git

*/

/*
	6. JB -done
7. Gyver -done
8. Carl
9. Cat
10. Mackoy
11. Emilio
12. James Lawrence
13. James M
14. Amzel
15. Quinnie
16. Aaron
17. Margaret
18. Mel
19. Faye
20. Den
21. Rommel
22. Ron
23. Jed
24. Marvin
25. Jamir

*/